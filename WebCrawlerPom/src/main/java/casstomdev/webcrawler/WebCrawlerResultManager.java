/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import casstomdev.webcrawler.results.ParsedContent;
import casstomdev.webcrawler.results.WebCrawlerResult;
import casstomdev.webcrawler.workers.URLManager;

/**
 *
 * @author T430
 */
public class WebCrawlerResultManager implements ResultManager {

    private static final int BUFF_SIZE = 100;
//    HashMap<String, WebCrawlerResult> results = new HashMap<>(1000);
    private final URLManager urlManager = new URLManager();
    ArrayList<String> currentProcessBuffer = new ArrayList<>(100);
    private final Semaphore StorageSem = new Semaphore(1);
    private final Semaphore BufferSem = new Semaphore(1);
    private final int PageNum;
    private String url;
//    private String[] resBuffer = new String[100];
    private WebCrawlerResult[] resBuffer = new WebCrawlerResult[BUFF_SIZE];

    private int idx = 0;

    public int currentProcessBufferSize() {
        return currentProcessBuffer.size();
    }

    /**
     *
     * @param PageNum number of pages
     */
    WebCrawlerResultManager(int PageNum) {
        this.PageNum = PageNum;

    }

    public void cleanOutput(String url) {
        URI uri;
        String tmp = "";
        try {
            uri = new URI(url);
            String domain = uri.getHost();
            domain = domain.startsWith("www.") ? domain.substring(4) : domain;
            tmp = domain + uri.getPath();

        } catch (URISyntaxException ex) {
            Logger.getLogger(WebCrawlerResultManager.class.getName()).
                    log(Level.SEVERE, null, ex);

        }
        FileWriter fw;
        this.url = tmp.replace("/", ".");
        try {
            fw = new FileWriter(WebCrawlerConfig.get(
                    "result_output_path") + "\\" + this.url + WebCrawlerConfig.
                    get(
                            "result_output_filename"));
            fw.write("");
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(WebCrawlerResultManager.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public boolean putProcess(String url) {
        boolean added = false;
        /*
         * if processed pages amount is less than defined maximum results
         */
//        if (PageNum < results.size() + currentProcessBuffer.size()) { //moze pod semafor to?
//            return added;
//        }
//        System.out.println("size" + this.urlManager.size());
        if (PageNum < this.getUrlManager().size() + currentProcessBuffer.size()) { //moze pod semafor to?
            return added;
        }
        try {
            if (url.endsWith("/")) {
                url = url.substring(0, url.length() - 1);
            }
            if (url.contains("#")) {
                url = url.substring(0, url.lastIndexOf('#'));
            }
//            System.out.println("begin put");
            BufferSem.acquire();
            if (!currentProcessBuffer.contains(url)) {
                StorageSem.acquire();

                boolean contains = getUrlManager().contains(url);
//                if (!results.containsKey(url)) {
//                    added = true;
//                    currentProcessBuffer.add(url);
//                }
                if (!contains) {
                    added = true;
//                    System.out.println("bbb " + url);
                    currentProcessBuffer.add(url);
                } else {
//                    System.out.println("aaa " + url);
                }
                StorageSem.release();
            }
            BufferSem.release();
//            System.out.println("end put");
        } catch (InterruptedException ex) {
            Logger.getLogger(WebCrawlerResultManager.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
        return added;

    }

    @Override
    public boolean addResult(WebCrawlerResult result) {
        boolean added = false;
//        System.out.println("begin2 add");
        try {
            StorageSem.acquire();
            if (!checkResult(result)) {
//                if (result.toString() != null) {
//                    results.put(result.getUrl(), result);
//                } else {
//                    results.put(result.getUrl(), null);
//                }
//                this.resBuffer[idx] = result;//.getUrl();
                if (result.toString() != null) {
                    this.resBuffer[idx] = result;//.getUrl();
                } else {
                    this.resBuffer[idx] = null;//.getUrl();
                }
                getUrlManager().addUrl(result.getUrl());
                idx++;
                if (this.idx >= this.resBuffer.length) {
                    this.dumpPartResults();
                }
                added = true;
            }
            currentProcessBuffer.remove(result.getUrl());
            StorageSem.release();

        } catch (Exception ex) {
            Logger.getLogger(WebCrawlerResultManager.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
//        System.out.println("end2 add");
        return added;
    }

    @Override
    public boolean checkResult(WebCrawlerResult result) {
        //        if (results.containsValue(result)) {
//            return true;
//        }
//        return false;
        return getUrlManager().contains(result.getUrl());
    }

    private void dumpPartResults() {
        try {

            FileWriter fw = new FileWriter(WebCrawlerConfig.get(
                    "result_output_path") + "\\" + this.url + WebCrawlerConfig.
                    get(
                            "result_output_filename"), true);
            fw.write(ParsedContent.structToString());
//            for (Map.Entry<String, WebCrawlerResult> res : results.entrySet()) {
//                if (res.getValue() != null) {
//                    fw.write(res.getValue().toString());
//                    this.results.put(res.getKey(), null);
//                }
//            } 
            //leave upper commented
//            for (String key : this.resBuffer) {
//                if (key != null) {                    
//                    if (results.get(key) != null) {
//                        fw.write(results.get(key).toString());
//                        this.results.put(key, null);
//                    }
//                }
//            }
            for (WebCrawlerResult res : this.resBuffer) {
                if (res != null) {
                    fw.write(res.toString());
                }
            }
//            this.resBuffer = new String[BUFF_SIZE];
            this.resBuffer = null;
            this.resBuffer = new WebCrawlerResult[BUFF_SIZE];
            this.idx = 0;
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(WebCrawlerResultManager.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }

    public void dumpResults() {
        try {

            FileWriter fw2 = new FileWriter(WebCrawlerConfig.get(
                    "result_output_path") + "\\" + this.url + "urls_" + WebCrawlerConfig.
                    get(
                            "result_output_filename"));
//            FileWriter fw = new FileWriter(WebCrawlerConfig.get(
//                    "result_output_path"), true);
//            for (Map.Entry<String, WebCrawlerResult> res : results.entrySet()) {
//                if (res.getValue() != null) {
//                    fw.write(res.getValue().toString());
//
//                }
//                fw2.write(res.getValue().getUrl() + "\n");
//                //TODO: zmienic format tego pliku
//            }
//            fw.write(ParsedContent.structToString());
//            fw.close();
            dumpPartResults();
            fw2.write(this.getUrlManager().urlToString());
            fw2.close();

        } catch (IOException ex) {
            Logger.getLogger(WebCrawlerResultManager.class.getName()).log(
                    Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the urlManager
     */
    public URLManager getUrlManager() {
        return urlManager;
    }
}
