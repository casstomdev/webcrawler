/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import casstomdev.webcrawler.workers.WebCrawlerObserver.Task;
import java.io.Console; 

/**
 *
 * @author Tomek
 */
public class WebCrawlerMain {

    private static int i = 0;

    public static void main(String[] argv) throws InterruptedException {

//        wct.Restart(null);
//        wct.Crawl("http://www.kuzniewski.pl/");
        Console c = System.console();
        boolean auto = true;
        System.out.println("Wstaw link:");
//        String readLine = c.readLine();
        String readLine = "http://www.kuzniewski.pl/";//"http://www.komputronik.pl/";
        readLine="http://moto.gratka.pl/";
        readLine="http://otomoto.pl/";
        WebCrawlerThreaded wct = new WebCrawlerThreaded();
        wct.Crawl(readLine);
//        readLine = c.readLine("Auto monitor (y/n): ");
        if (readLine.equals("n")) {
            auto = false;
        }
        System.out.println("Pomoc: 'help'");
        //"http://www.komputronik.pl/"
        while (!wct.isFinished()) {
            if (auto) {
                autoPrint(wct);
            } else {
                try {
                    readLine = c.readLine("komenda: ");
                    if (readLine.equals("help")) {
                        System.out.println("lista komend:");
                        System.out.println("clean");
                        System.out.println("workerNum");
                        System.out.println("size");
                        System.out.println("urlSize");
                        System.out.println("memory");
                    } else {

                        try {
                            Task task = Task.valueOf(readLine);
                            wct.observer.observe(task);
                        } catch (Exception e) {
                            System.out.println("Zła komenda ('help')");
                        }
                    }
                } catch (Exception e) {

                }

            }

        }

//        WebCrawler c = new WebCrawler();
//        c.Crawl();
    }

    private static void autoPrint(WebCrawlerThreaded wct) throws InterruptedException {
        Thread.sleep(10000);
        if (i % 4 == 0) {
            wct.observer.observe(Task.workerNum);
        }
        if (i == 0) {
            wct.observer.observe(Task.size);
            wct.observer.observe(Task.memory);
            wct.observer.observe(Task.clean);
            wct.observer.observe(Task.memory);
        }
        wct.observer.observe(Task.urlSize);
        i++;
        i %= 18;
    }

    private static boolean endApp(String input, WebCrawlerThreaded wct) {
        boolean res = false;
        if (wct != null) {
            res = wct.isFinished();
        }
        if (input != null) {
            if (input.equals("exit")) {
                return true;
            }
        }
        return false;
    }

}
