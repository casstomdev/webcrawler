///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package casstomdev.webcrawler.logger;
//
//import casstomdev.webcrawler.WebCrawlerConfig;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.concurrent.Semaphore;
//import java.util.logging.Level;
//
///**
// *
// * @author T430
// */
//public final class Log {
//
//    String[] buffer;
//    public String logid;
//    Semaphore writeSem = new Semaphore(1);
//    int idx = 0;
//
//    /*
//    * save action to buffer
//     */
//    public void Log(String msg) {
//        try {
//            writeSem.acquire();
//            if (idx >= buffer.length) {
//                saveLog();
//            }
//            buffer[idx++] = msg;
//            writeSem.release();
//        } catch (InterruptedException ex) {
//            java.util.logging.Logger.getLogger(WebCrawlerLogger.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public Log(int buffSize, String id) {
//        initLog(buffSize, id);
//    }
//
//    private void initLog(int buffSize, String id) {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
//        Date date = new Date();
//        this.logid = "crawler-" + dateFormat.format(date) + "-" + id + ".log";
//        this.buffer = new String[buffSize];
//    }
//
//    /*
//    * save current log buffer to file
//     */
//    public void storeLog() {
//        try {
//            writeSem.acquire();
//
//            saveLog();
//
//            writeSem.release();
//        } catch (InterruptedException ex) {
//            java.util.logging.Logger.getLogger(WebCrawlerLogger.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * save buffer to file.
//     */
//    private void saveLog() {
//        FileWriter fw = null;
//        try {
//            fw = new FileWriter(WebCrawlerConfig.get("log_directory_path") + File.separator + logid, true);
//            for (int i = 0; i < idx; i++) {
//                fw.write(buffer[i] + "\n");
//            }
//            idx = 0;
//
//        } catch (IOException ex) {
//            java.util.logging.Logger.getLogger(WebCrawlerLogger.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                fw.close();
//            } catch (IOException ex) {
//                java.util.logging.Logger.getLogger(WebCrawlerLogger.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//    
//}
