/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import casstomdev.webcrawler.WebCrawler;
import casstomdev.webcrawler.results.WebCrawlerResult;

//TODO: dodac poziom zaglębienia do logu
/**
 *
 * @author T430
 */
public class RecoverLog {

    /**
     * {url, page_content}>
     */
    static HashMap<String, String> content;
    /**
     * tasks to restart
     */
    public static ArrayList<String> tasks = new ArrayList<>();

    /*
    * restart crawling process from log file (only urls)
     */
    public static void restartTask(String logPath, WebCrawler crawler) {
        readLog(logPath, crawler);
    }

    /*
    * restart crawling process from log file (only urls)
     */
    public static void restartTask(String logPath, String contentPath, WebCrawler crawler) {
        readContent(contentPath);
        readLog(logPath, crawler);
    }

    /**
     * run task line
     *
     * @param line = flag;date;url
     */
    private static void runLine(String line, WebCrawler crawler) {
        String[] fields = line.split(";");
        switch (fields[0]) {
            case "1":
                String webContent = null;
                if (content != null) {
                    webContent = content.get(fields[2]);
                }

                WebCrawlerResult res;
                if (webContent != null) {
                    res = new WebCrawlerResult(fields[2], webContent);
                } else {
                    res = new WebCrawlerResult(fields[2]);
                }
                crawler.getCrawlingResults().addResult(res); //TODO:
                break;
            case "0":
                //  crawler.getCrawlingResults().putProcess(fields[2]);
                tasks.add(fields[2]);
                break;
        }
    }

    /**
     * read log file and prepares tasks
     *
     * @param logPath
     * @param crawler
     */
    private static void readLog(String logPath, WebCrawler crawler) {
        try {
            //  ArrayList<String> log = new ArrayList<>(1000);
            FileReader fr;
            fr = new FileReader(logPath);
            int c = fr.read();
            String line = "";
            while (c > -1) {
                if (c == '\n') {
                    runLine(line, crawler);
                    //   log.add(line); 
                    line = "";
                } else {
                    line += (char) c;
                }
                c = fr.read();
            }
        } catch (IOException ex) {
            Logger.getLogger(RecoverLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //TODO: problem 2 logow, jesli zrobimy restart to stworzymy rowniez nowy log
    /**
     * reads web content from file
     *
     * @param contentPath
     */
    private static void readContent(String contentPath) {
        content = new HashMap<>();
        try {
            //  ArrayList<String> log = new ArrayList<>(1000);
            FileReader fr;
            fr = new FileReader(contentPath);
            int c = fr.read();
            String line = "";
            while (c > -1) {
                if (c == '\n') {
                    String[] fields = line.split(";");
                    if (fields.length == 2) {
                        content.put(fields[0], fields[1]);
                    }
                    //   log.add(line); 
                    line = "";
                } else {
                    line += c;
                }
                c = fr.read();
            }
        } catch (IOException ex) {
            Logger.getLogger(RecoverLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
