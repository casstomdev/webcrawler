/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.logger;

import com.casstomdev.utils.ILogger;
import com.casstomdev.utils.LoggerImpl;

/**
 *
 * @author T430
 */
public class WCLogger {

    private static final ILogger logger = new LoggerImpl(WCLogger.class);

    public static ILogger getLogger() {
        return logger;
    }
 
    /**
     * synchronized println
     *
     * @param msg msg to print
     */
    public static void printMsg(String msg) {
        synchronized (System.class) {
            System.out.println(msg);
        }
    }

 
}
