package casstomdev.webcrawler.logger;

import casstomdev.webcrawler.WebCrawlerConfig;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 *
 * @author casstomdev
 */
public class WebCrawlerLogger {

    static WebCrawlerMessage[] buffer;
    public static String logid;
    static Semaphore writeSem = new Semaphore(1);
    static int idx = 0;

    /*
    * save action to buffer
     */
    public static void LogStart(String url) {
        try {
            writeSem.acquire();
            if (idx >= buffer.length) {
                saveLog();
            }
            buffer[idx++] = new WebCrawlerMessage(WebCrawlerMessage.MessageType.START, url);
            writeSem.release();
        } catch (InterruptedException ex) {
            WCLogger.getLogger().logException(ex);
        }
    }

    /*
    * save action to buffer
     */
    public static void LogEnd(String url) {
        try {
            writeSem.acquire();
            if (idx >= buffer.length) {
                saveLog();
            }
            buffer[idx++] = new WebCrawlerMessage(WebCrawlerMessage.MessageType.END, url);
            writeSem.release();
        } catch (InterruptedException ex) {
            WCLogger.getLogger().logException(ex);
        }
    }

    public static void initLog(int buffSize, String id) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date date = new Date();
        logid = "crawler-" + dateFormat.format(date) + "-" + id + ".log";
        buffer = new WebCrawlerMessage[buffSize];

    }

    /*
    * save current log buffer to file
     */
    public static void storeLog() {
        try {
            writeSem.acquire();

            saveLog();

            writeSem.release();
        } catch (InterruptedException ex) {
            WCLogger.getLogger().logException(ex);
        }
    }

    /**
     * save buffer to file.
     */
    private static void saveLog() {
        FileWriter fw = null;
        try {
            fw = new FileWriter(WebCrawlerConfig.get("log_directory_path") + "\\" + logid, true);
            for (int i = 0; i < idx; i++) {
                fw.write(buffer[i].toString() + "\n");
            }
            idx = 0;

        } catch (IOException ex) {
            WCLogger.getLogger().logException(ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                WCLogger.getLogger().logException(ex);
            }
        }
    }

    public static class WebCrawlerMessage {

        private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        public class MessageType {

            public static final int START = 0;
            public static final int END = 1;
        };
        int type;
        String url;
        Date date;

        /**
         *
         * @param type WebCrawlerMessage.MessageType
         * @param url
         */
        public WebCrawlerMessage(int type, String url) {
            this.type = type;
            this.url = url;
            this.date = new Date();
        }

        @Override
        public String toString() {
            return this.type + ";" + df.format(this.date) + ";" + this.url;
        }

    }
}
