/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.workers;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import casstomdev.webcrawler.WebCrawler;
import casstomdev.webcrawler.WebCrawlerConfig;
import webcrawler.utils.WebCrawlerPath;

/**
 *
 * @author T430
 */
public class WorkerManager {

   ThreadPoolExecutor tpes;
   /**
    * max thead num
    */
   int workersNum;
   Semaphore addSem = new Semaphore(1);
   WebCrawler Crawler;

   public WorkerManager() {

      int threadNum = Integer.parseInt(WebCrawlerConfig.get("max_thread_num"));
      this.tpes = new ScheduledThreadPoolExecutor(threadNum);
      this.workersNum = threadNum;

   }

   public WorkerManager(WebCrawler crawler) {
      int threadNum = Integer.parseInt(WebCrawlerConfig.get("max_thread_num"));
      this.tpes = new ScheduledThreadPoolExecutor(threadNum);
      this.workersNum = threadNum;
      this.Crawler = crawler;
   }

   /**
    * add task to thread pool
    *
    * @param Url nect url
    * @param path previous node path
    */
   public void addTask(String Url, WebCrawlerPath path) {
      Worker worker = new Worker(Url, path, this);
      tpes.execute(worker);
   }

   /**
    * checks if current thread can delegate task to next thread
    *
    * @param distance distance from root
    * @return true if task can be delegated
    */
   public boolean checkPoolOccupancy(int distance) {
      double ratio = Double.parseDouble(WebCrawlerConfig.get("thread_pool_occupancy_ratio"));
      double value = Math.pow(ratio, distance - 1);
      if (value > tpes.getActiveCount() / this.workersNum) {
         return true;
      }
      return false;
   }

   /**
    * get number of working threads
    *
    * @return
    */
   public int getPoolOccupancy() {
      return this.tpes.getActiveCount();
   }

   /**
    * closes thread pool and waits for remaining tasks 1 day
    */
   public void close() {
      try {
         tpes.shutdown();//czeka na zamkniecie kazdego
         tpes.awaitTermination(1, TimeUnit.DAYS);
      } catch (InterruptedException ex) {
         Logger.getLogger(WorkerManager.class.getName()).log(Level.SEVERE, null, ex);
      }
   }
}
