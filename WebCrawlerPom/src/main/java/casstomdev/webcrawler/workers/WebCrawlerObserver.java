/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.workers;

import casstomdev.webcrawler.WebCrawlerThreaded;
import casstomdev.webcrawler.logger.WCLogger;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author T430
 */
public class WebCrawlerObserver extends Thread {

    private final Semaphore semObserver = new Semaphore(1);
    private final Semaphore semManager = new Semaphore(1);
    private boolean stop = false;
    private WebCrawlerThreaded WCT;

    public enum Task {

        workerNum, urlSize, size, memory, clean
    };
    private Task task;

    public WebCrawlerObserver(WebCrawlerThreaded wct) {
        this.WCT = wct;
        try {
            semObserver.acquire();
        } catch (InterruptedException ex) {

            WCLogger.getLogger().error("observer error");
            WCLogger.printMsg("observer does not work");
            //  Logger.getLogger(WebCrawlerObserver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                semObserver.acquire();
                if (!stop) {
                    switch (task) {
                        case workerNum:
                            _printWorkerNum();
                            break;
                        case urlSize:
                            _printUrlSize();
                            break;
                        case size:
                            _printSize();
                            break;
                        case memory:
                            _memoryUsage();
                            break;
                        case clean:
                            _clean();
                            break;

                    }
                }
                semManager.release();
            } catch (InterruptedException ex) {
                WCLogger.getLogger().error("observer error");
                WCLogger.printMsg("observer does not work");
                //  Logger.getLogger(WebCrawlerObserver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public synchronized void observe(Task task) {
        try {
            semManager.acquire();
            this.task = task;
            semObserver.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(WebCrawlerObserver.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    /**
     * stops observation
     */
    public void stopObservation() {
        stop = true;
        semObserver.release();
    }

    /**
     * prints current working threads
     */
    private void printWorkerNum() {

    }

    private void _printUrlSize() {
        String msg;
        msg = "url size: " + this.WCT.getCrawlingResults().getUrlManager().
                size();
        WCLogger.printMsg(msg);

    }

    private void _printWorkerNum() {
        String msg;
        msg = "worker num: " + this.WCT.getWM().getPoolOccupancy();
        WCLogger.printMsg(msg);

    }

    private void _printSize() {

        String msg;

//        String s = this.WCT.getCrawlingResults().getUrlManager().toString();
//        msg = " res size: " + this.WCT.getCrawlingResults().
//                getUrlManager().sizeOff();
//        WCLogger.printMsg(msg);
        msg = " current buffer size: " + this.WCT.getCrawlingResults().
                currentProcessBufferSize();
        WCLogger.printMsg(msg);

    }

    public static void _memoryUsage() {
        int mb = 1024 * 1024;
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();

        System.out.println("##### Heap utilization statistics [MB] #####");

        //Print used memory
        System.out.println("Used Memory:"
                + (runtime.totalMemory() - runtime.freeMemory()) / mb);

        //Print free memory
        System.out.println("Free Memory:"
                + runtime.freeMemory() / mb);

        //Print total available memory
        System.out.println("Total Memory:" + runtime.totalMemory() / mb);

        //Print Maximum available memory
        System.out.println("Max Memory:" + runtime.maxMemory() / mb);
    }

    private static void _clean() {
        System.gc();
        System.runFinalization();
    }

}
