/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.workers;

import casstomdev.webcrawler.WebCrawlerConfig;
import casstomdev.webcrawler.WebCrawlerFunctions;
import casstomdev.webcrawler.logger.WebCrawlerLogger;
import casstomdev.webcrawler.results.ParsedContent;
import casstomdev.webcrawler.results.ResultParser;
import casstomdev.webcrawler.results.WebCrawlerResult;
import casstomdev.webcrawler.utils.filters.FilterManager;
import java.util.List;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import webcrawler.utils.WebCrawlerPath;

/**
 *
 * @author T430
 */
public class Worker implements Runnable {

    WebCrawlerPath Path;
    WorkerManager Manager;

    Worker(String Url, WebCrawlerPath path, WorkerManager manager) {
        this.Path = new WebCrawlerPath(path);
        this.Path.add(Url);
        this.Manager = manager;
    }

    @Override
    public void run() {
        WorkerCrawl(this.Path);

    }

    public void WorkerCrawl(WebCrawlerPath path) throws NumberFormatException {
//        System.out.println("C:"+path.getCurrent().getUrl() + " "+path.size());
//        if(path.getCurrent().getUrl().contains("http://moto.gratka.pl/osobowe/?s")){
//            System.out.println("get list");
//        }
//        if (path.getCurrent().getUrl().contains(
//                "http://otomoto.pl/osobowe/?page=")) {
//            System.out.println("get list");
//        }
        int depth = Integer.parseInt(WebCrawlerConfig.get("crawl_depth"));
        if (path.size() < depth) {
            boolean putProcess = Manager.Crawler.getCrawlingResults().
                    putProcess(
                            path.getCurrent().getUrl());

//        System.out.println("putProcess");
            if (putProcess) {
                WebCrawlerLogger.LogStart(path.getCurrent().getUrl());
                WebCrawlerFunctions wcf = new WebCrawlerFunctions(path);
//            System.out.println("startProcess:(" + path.size() + ") " + path.
//                    getCurrent().getUrl());

                String content = "";

                if (wcf.getPage()) {
                    content = wcf.getPageContent();
                    Elements urlList = wcf.getUrlList();
                    boolean emptyUrl = true;
                    for (Element url : urlList) {
                        if (FilterManager.checkURL(url.attr("abs:href"),
                                this.Manager.Crawler)) {
                            emptyUrl = false;
                            if (Manager.checkPoolOccupancy(path.size())) {
                                this.Manager.addTask(url.attr("abs:href"), path);
                            } else {
                                WebCrawlerPath tmp = new WebCrawlerPath(path);
                                tmp.add(url.attr("abs:href"));
                                WorkerCrawl(tmp);

                            }
                        }

                    }
                    if (emptyUrl) {
//                        System.out.println("Empty: " + path.getCurrent().
//                                getUrl());
                    }
                }

                WebCrawlerResult res = new WebCrawlerResult(path.getCurrent().
                        getUrl(), content);

                if (WebCrawlerConfig.getParser() != null) {
                    ResultParser parser = WebCrawlerConfig.getParser();
                    List<ParsedContent> parse = parser.parse(wcf.
                            getJsoupDocument(),
                            wcf.getUrl());
                    res.setParsedContent(parse);
                }
                Manager.Crawler.getCrawlingResults().addResult(res);
                //sprawdzac czy powyzsze adresy znajduja sie w pliku logu ( obojetnie czy w process list czy finished list)
                WebCrawlerLogger.LogEnd(path.getCurrent().getUrl());
//            System.out.println("finish: " + path.getCurrent().getUrl());

            }
        } else {
            //   System.out.println("(" + depth + ")depth: " + path.size());
        }
    }
}
