/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.workers;

import java.util.Date;

/**
 *
 * @author T430
 */
public class URLManager {

    UrlTrieNode root = new UrlTrieNode();
    private int size = 0;

    /**
     * @return the size
     */
    public int size() {
        return size;
    }

    class UrlTrieNode {

        private UrlTrieNode[] children;
        private String suffix;
        private boolean end = false;

        /**
         *
         * @param parsedUrl
         * @param idx
         * @return false url was indexed
         */
        public boolean addUrl(String parsedUrl, int idx) {
            if (idx == parsedUrl.length()) {
                boolean res = !end;
                end = true;
                return res;
            }
            if (children == null && suffix == null) {
                this.suffix = parsedUrl.substring(idx, parsedUrl.length());
            } else if (children == null) {
                if (!parsedUrl.substring(idx, parsedUrl.length()).equals(suffix)) {
                    splitNode(parsedUrl, idx);
                } else {
                    return false;
                }
            } else {
                UrlTrieNode child = this.selectChild(parsedUrl.charAt(idx));
                if (child == null) {

                    child = new UrlTrieNode();
                    children[this.getIndex(parsedUrl.charAt(idx))] = child;
                }
                return child.addUrl(parsedUrl, idx + 1);
            }
            return true;
        }

        /**
         *
         * @param parsedUrl
         * @param idx
         * @return false url was indexed
         */
        public boolean contains(String parsedUrl, int idx) {
            if (idx == parsedUrl.length()) {
                return end;
            }
            if (children == null && suffix == null) {
                return false;
            } else if (children == null) {
                if (!parsedUrl.substring(idx, parsedUrl.length()).equals(suffix)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                UrlTrieNode child = this.selectChild(parsedUrl.charAt(idx));
                if (child == null) {
                    return false;
                }
                return child.contains(parsedUrl, idx + 1);
            }

        }

        private UrlTrieNode selectChild(char c) {
            int index = this.getIndex(c);
            return this.children[index];
        }

        private int getIndex(char c) {
            if (c >= 97 && c <= 122) {
                return c - 97;
            }//26
            if (c == '_') {
                return c - 69;//1
            }
            if (c == '-') {
                return c - 38;//1
            }
            if (c >= 48 && c <= 57) {
                return c - 20;
            }
            return -1;
        }

        public char getChar(int index) {
            if (index < 26) {
                return (char) (index + 97);
            }//26
            if (index == 27) {
                return (char) (index + 69);//1
            }
            if (index == 28) {
                return (char) (index + 38);//1
            }

            return (char) (index + 20);

        }

        private void splitNode(String parsedUrl, int idx) {

            children = new UrlTrieNode[38];

            int i = getIndex(suffix.charAt(0));
            int j = getIndex(parsedUrl.charAt(idx));
            UrlTrieNode child1 = new UrlTrieNode();
            children[i] = child1;
            child1.addUrl(suffix, 1);

            if (i == j) {
                child1.addUrl(parsedUrl, idx + 1);
            } else {
                UrlTrieNode child2 = new UrlTrieNode();
                children[j] = child2;
                child2.addUrl(parsedUrl, idx + 1);
            }
//            UrlTrieNode child2 = new UrlTrieNode();
//            children[i] = child2;
//            child2.addUrl(suffix, 1);
            suffix = null;
        }

    }

    public boolean contains(String url) {
        url = resolveUrl(url);
        return root.contains(url, 0);
    }

    public boolean addUrl(String url) {
        url = resolveUrl(url);
        boolean addUrl = root.addUrl(url, 0);
        if (addUrl) {
            this.size++;
        }
        return addUrl;
    }

    public static String resolveUrl(String url) {
        if (url.startsWith("https")) {
            url = url.substring(5);
        } else if (url.startsWith("http")) {
            url = url.substring(4);
        }
        final char[] toCharArray = url.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < toCharArray.length; i++) {
            char c = toCharArray[i];
            c = Character.toLowerCase(c);
            if ((c >= 97 && c <= 122)//26
                    || c == '-' || c == '_' //2
                    || (c >= 48 && c <= 57)) { //10
                sb.append(c);

            }
        }
        url = sb.toString();
        if (url.startsWith("www")) {
            url = url.substring(3);
        }
        if (url.endsWith("html")) {
            url = url.substring(0, url.length() - 4);
        } else if (url.endsWith("php")) {
            url = url.substring(0, url.length() - 3);
        }
        return url;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb = trieToString(this.root, sb, 0);
        return sb.toString();
    }

    public int sizeOff() {
        int size = trieSize(this.root, 0);
        return size;
    }

    private StringBuilder trieToString(UrlTrieNode node, StringBuilder sb,
            int indent) {
        if (node.children == null) {
            if (node.suffix != null) {
                sb.append(printIndent(indent)).append(node.suffix).append(
                        "\n");
            }
            return sb;
        }
        for (int i = 0; i < node.children.length; i++) {
            if (node.children[i] != null) {
                sb.append(printIndent(indent)).append(node.getChar(i)).append(
                        "\n");
                sb = trieToString(node.children[i], sb, indent + 1);
            }
        }
        return sb;
    }

    private int trieSize(UrlTrieNode node, int size) {
        if (node.children == null) {
            if (node.suffix != null) {
                size += node.suffix.length() + 50;
            }
            return size;
        }
        for (int i = 0; i < node.children.length; i++) {
            size += 10;
            if (node.children[i] != null) {

                size = trieSize(node.children[i], size);
            }
        }
        return size;
    }

    private String printIndent(int indent) {
        String res = "";
        for (int i = 0; i < indent; i++) {
            res += "\t";
        }
        return res + "-";
    }

    public String urlToString() {
        StringBuilder sb = new StringBuilder();
        sb = trieUrlsToString(this.root, sb, "");
        return sb.toString();
    }

    private StringBuilder trieUrlsToString(UrlTrieNode node, StringBuilder sb,
            String tmp) {
        if (node.children == null) {
            if (node.suffix != null) {
                sb.append(tmp).append(node.suffix).append("\n");
            } else {
                sb.append(tmp).append("\n");
            }
            return sb;
        }
        if (node.end) {
            sb.append(tmp).append("\n");

        }
        for (int i = 0; i < node.children.length; i++) {
            if (node.children[i] != null) {
                sb = trieUrlsToString(node.children[i], sb, tmp + node.
                        getChar(i));
            }
        }
        return sb;
    }

}
