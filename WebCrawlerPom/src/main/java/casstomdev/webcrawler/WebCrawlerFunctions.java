/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import casstomdev.webcrawler.logger.WCLogger;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import webcrawler.utils.WebCrawlerPath;

/**
 *
 * @author T430
 */
public class WebCrawlerFunctions {

    WebCrawlerPath currentPath;
    WebCrawler Crawler;
    private Document jsoupDocument;
    private String url;

    public WebCrawlerFunctions(String url, WebCrawlerPath path,
            WebCrawler crawler) {
        currentPath = new WebCrawlerPath(path);
        this.Crawler = crawler;

    }

    public WebCrawlerFunctions(WebCrawlerPath path) {
        currentPath = new WebCrawlerPath(path);

    }

    /**
     * downloads page
     *
     * @return true if everything was ok, false if page dont exists
     */
    public boolean getPage() {
        for (int i = 0; i < WebCrawlerConfig.getInt("max_retry_num", 1); i++) {
            try {
                int timeout = Integer.parseInt(WebCrawlerConfig.get("timeout"));
                this.url = currentPath.getCurrent().getUrl();
                jsoupDocument = Jsoup.connect(currentPath.getCurrent().getUrl()).
                        timeout(timeout).userAgent(
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/40.0").
                        get();
                return true;
            } catch (IOException ex) {
                //         WCLogger.getLogger(WebCrawlerFunctions.class.getName()).log(Level.SEVERE, null, ex);
                if (i < WebCrawlerConfig.getInt("max_retry_num", 1) - 1) {
                    WCLogger.getLogger().error("jsoup",
                            "jsoup problem- retry " + currentPath.getCurrent().getUrl());
                }

            }

        }
        WCLogger.printMsg("jsoup error");
        WCLogger.getLogger().error("jsoup", "jsoup problem " + currentPath.getCurrent().getUrl());
        return false;
        //TODO przechwycic wyjatek
    }
//TODO:semantic data

    /**
     * gets url List
     *
     * @return
     */
    public Elements getUrlList() {
        //TODO: url filter

        Elements urls = getJsoupDocument().select("a[href]");
        return urls;
    }

    /**
     * gets pages text
     *
     * @return
     */
    public String getPageContent() {
        return this.getJsoupDocument().text();
    }

    /**
     * @return the jsoupDocument
     */
    public Document getJsoupDocument() {
        return jsoupDocument;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

}
