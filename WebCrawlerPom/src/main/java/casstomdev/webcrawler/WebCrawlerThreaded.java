/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import casstomdev.webcrawler.workers.WebCrawlerObserver;

/**
 *
 * @author T430
 */
public class WebCrawlerThreaded extends WebCrawler {

    /**
     * if crawling was finished
     */
    private boolean finished = true;
    
    public WebCrawlerThreaded(String path) {
        super(path);
    }
    WebCrawlerObserver observer;
    
    public WebCrawlerThreaded() {
        super();
        this.observer = new WebCrawlerObserver(this);
        
    }
    
    @Override
    public void Crawl(String startPoint) {
        this.WCRM.cleanOutput(startPoint);
        if (this.isFinished()) { //TODO: zrobic stop
            this.finished = false;
            Crawler crawler = new Crawler(this, startPoint, false);
            crawler.start();
            this.observer.start();
        }
    }
    
    @Override
    public void Restart(String startPoint) {
        if (this.isFinished()) { //TODO: zrobic stop
            this.finished = false;
            Crawler crawler = new Crawler(this, startPoint, true);
            crawler.start();
            this.observer.start();
        }
    }

//    /**
//     * prints number of current working threads
//     */
//    public void getWorkerNum() {
//        observer.printWorkerNum();
//    }
    
    private void stop() {
        this.finished = true;
        observer.stopObservation();
    }

    /**
     * @return the finished
     */
    public boolean isFinished() {
        return finished;
    }
    
    private class Crawler extends Thread {
        
        private WebCrawlerThreaded WCT;
        private String startPoint;
        private boolean Restart;

        /**
         *
         * @param WCT
         * @param startPoint
         * @param restart if restart tasks
         */
        public Crawler(WebCrawlerThreaded WCT, String startPoint,
                boolean restart) {
            this.WCT = WCT;
            this.startPoint = startPoint;
            this.Restart = restart;
        }
        
        @Override
        public void run() {
            if (!Restart) {
                WCT._Crawl(startPoint);
            } else {
                WCT._Restart(startPoint);
            }
            System.out.println("Stopping....");
            WCT.stop();
        }
    }
}
