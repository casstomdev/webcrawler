/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import casstomdev.webcrawler.logger.WCLogger;
import casstomdev.webcrawler.logger.WebCrawlerLogger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import casstomdev.webcrawler.results.CSSParser;
import casstomdev.webcrawler.results.ResultParser;
import casstomdev.webcrawler.utils.ValueValidators;
import casstomdev.webcrawler.utils.filters.FilterManager;

/**
 *
 * @author T430
 */
public class WebCrawlerConfig {

    private static Properties config = new Properties();
    private static ResultParser parser;

    /**
     * reads config from file path
     *
     * @param configPath
     */
    public static void openConfigFile(String configPath) {
        File configFile = new File(configPath);
        if (configFile.exists()) {
            try {
                config.load(new FileInputStream(configPath));
            } catch (IOException ex) {
                Logger.getLogger(WebCrawlerConfig.class.getName()).log(
                        Level.SEVERE, null, ex);
                WebCrawlerDefaultConfig wdc = new WebCrawlerDefaultConfig();
                config = wdc.getDefaultConfig();
            }
        } else {
            WebCrawlerDefaultConfig wdc = new WebCrawlerDefaultConfig();
            config = wdc.getDefaultConfig();

        }
        checkDirectory("log_directory_path", true);
        checkDirectory("url_output_path", false);
        checkDirectory("result_output_path", false);
        String s = config.getProperty("result_output_filename");
        if (s != null && s.length() < 1) {
            s = "out";
        }
    }

    /**
     * init crawlers structures
     */
    public static void init() {
        int buffSize = Integer.parseInt(config.
                getProperty("max_log_buffer_size"));
        WebCrawlerLogger.initLog(buffSize, config.getProperty(
                "log_id"));
        //   WCLogger.initLogs();
        try {
            ValueValidators.intcheck(WebCrawlerConfig.get("max_thread_num"), 0,
                    10000, "max thread num error value use between 0 , 10000");
            ValueValidators.doublecheck(WebCrawlerConfig.get(
                    "thread_pool_occupancy_ratio"), 0.0, 1.0,
                    "thread_pool_occupancy_ratio error value use between (0.0 , 1.0)");
            ValueValidators.intcheck(WebCrawlerConfig.get("timeout"), 5000,
                    600000, "timeout error value use between (5000, 600000)");
            ValueValidators.
                    intcheck(WebCrawlerConfig.get("crawl_depth"), 1, 100,
                            "timeout error value use between (1, 100)");
        } catch (Exception ex) {
            Logger.getLogger(WebCrawlerConfig.class.getName()).log(Level.SEVERE,
                    null, ex.getMessage());
        }
        FilterManager.importUrlFilter();
        initResultParser();

    }

    /**
     * @return the parser
     */
    public static ResultParser getParser() {
        return parser;
    }

    private static void initResultParser() {

        try {
            try (BufferedReader br = new BufferedReader(new FileReader(
                    WebCrawlerConfig.get("parser_path")))) {
                String line;
                //TODO: read parser type
                CSSParser p = new CSSParser();
                while ((line = br.readLine()) != null) {
                    String[] split = line.split("::");
                    CSSParser.CSSPathType type = CSSParser.CSSPathType.valueOf(
                            split[0]);
                    p.addPath(split[1], split[2], type);//TODO: read path type
                }
                parser = p;
            }

        } catch (IOException | IndexOutOfBoundsException ex) {
            java.util.logging.Logger.getLogger(WebCrawlerConfig.class.getName()).
                    log(Level.SEVERE, null, ex);

        } finally {

        }
    }

    /**
     * get config value
     *
     * @param key config name
     * @return
     */
    public static String get(String key) {
        return config.getProperty(key);
    }

    public static Integer getInt(String key, Integer def) {
        try {
            return Integer.parseInt(config.getProperty(key));
        } catch (NumberFormatException | NullPointerException e) {
            return def;
        }
    }

    public static Integer getInt(String key) {
        return getInt(key, null);
    }

    /**
     * gets current date
     *
     * @return date format - yyyy-MM-dd
     */
    public static String curDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * creates needed directories
     *
     * @param pathName
     * @param isDir true -> input path is only directory, false, path to file
     */
    private static void checkDirectory(String pathName, boolean isDir) {
        String path = WebCrawlerConfig.get(pathName);
        String[] dirPath = path.split("\\\\");
        int max = dirPath.length;
        if (!isDir) {
            max--;
        }

        String tmpPath = "";
        for (int i = 0; i < max; i++) {
            tmpPath += dirPath[i] + "\\";
            File dir = new File(tmpPath);
            if (!dir.exists()) {
                dir.mkdir();
            }
        }

//      } else {
//         int i = path.length() - 1;
//         char c = path.charAt(i--);
//         while (i >= 0 && c != '\\') {
//            c = path.charAt(i--);
//         }
//         if (i != 0) {
//            String dirPath = path.substring(0, i + 1);
//            File dir = new File(dirPath);
//            if (!dir.exists()) {
//               dir.mkdir();
//            }
//         }
        //   }
    }
}
