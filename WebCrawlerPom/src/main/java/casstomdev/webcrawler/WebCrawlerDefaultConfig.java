/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author T430
 */
public class WebCrawlerDefaultConfig {

    private Properties defaultConfig = new Properties();

    /**
     * creates and stores default config
     */
    public WebCrawlerDefaultConfig() {
        defaultConfig.setProperty("max_thread_num", "15");
        defaultConfig.setProperty("mtn", "15");

        defaultConfig.setProperty("timeout", "12000");
        defaultConfig.setProperty("to", "12000");

        defaultConfig.setProperty("max_retry_num", "5");
        defaultConfig.setProperty("mrn", "5");

        defaultConfig.setProperty("max_page_limit", "1000");
        defaultConfig.setProperty("mpl", "1000");

        defaultConfig.setProperty("crawl_depth", "5");
        defaultConfig.setProperty("cd", "5");

        defaultConfig.setProperty("max_log_buffer_size", "30");
        defaultConfig.setProperty("mlbs", "30");

        defaultConfig.setProperty("log_id", "webcrawler");
        defaultConfig.setProperty("li", "webcrawler");

        defaultConfig.setProperty("webcrawler_url_filter", "0");
        defaultConfig.setProperty("wuf", "0");

        defaultConfig.setProperty("webcrawler_html_tags", "1");
        defaultConfig.setProperty("wht", "1");

        defaultConfig.setProperty("webcrawler_data_pattern", "0");
        defaultConfig.setProperty("wdp", "0");

        defaultConfig.setProperty("thread_pool_occupancy_ratio", "0.5");
        defaultConfig.setProperty("tpor", "0.5");

        defaultConfig.setProperty("filter_path", "filters.config");
        defaultConfig.setProperty("fp", "filters.config");

        defaultConfig.setProperty("parser_path", "parser.config");
        defaultConfig.setProperty("parser_type", "css");
        defaultConfig.setProperty("output_format", "csv");

        defaultConfig.setProperty("url_output_path", "output\\url_list.out");
        defaultConfig.setProperty("uop", "output\\url_list.out");

        defaultConfig.setProperty("result_output_path", "output\\res_list.out");
        defaultConfig.setProperty("rop", "output\\res_list.out");

        defaultConfig.setProperty("log_directory_path", "logs");
        defaultConfig.setProperty("ldp", "logs");

        FileOutputStream fos;
        try {
            fos = new FileOutputStream("default.config");
            defaultConfig.store(fos, "default config");
        } catch (IOException ex) {
            Logger.getLogger(WebCrawlerDefaultConfig.class.getName()).log(
                    Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the defaultConfig
     */
    public Properties getDefaultConfig() {
        return defaultConfig;
    }
}
