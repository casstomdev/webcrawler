/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import casstomdev.webcrawler.results.WebCrawlerResult;

/**
 *
 * @author T430
 */
public interface ResultManager {

   /**
    * puts task to buffer
    *
    * @param url
    * @return true task has been put to buffer
    */
   boolean putProcess(String url);

   /**
    * add result to buffer
    *
    * @param result
    * @return true result added to list
    */
   boolean addResult(WebCrawlerResult result);

   /**
    * check if result exists
    *
    * @param result
    * @return true exists, false - doesn't exist
    */
   boolean checkResult(WebCrawlerResult result);
}
