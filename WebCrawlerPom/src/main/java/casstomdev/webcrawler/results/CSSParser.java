/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.results;

import casstomdev.webcrawler.WebCrawlerConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements; 

/**
 *
 * @author Tomek
 */
public class CSSParser extends ResultParser {

    List<CSSPath> cssPathList = new ArrayList<>();

    public enum CSSPathType {

        table, img, text, list, list_2
    }

    class CSSPath {

        String path;
        String label;
        CSSPathType pathType;

        public CSSPath(String label, String path, CSSPathType pathType) {
            this.path = path;
            this.pathType = pathType;
            this.label = label;
        }

    }

    public void addPath(String label, String path, CSSPathType type) {
        cssPathList.add(new CSSPath(label, path, type));
    }

    /**
     * delete all paths
     */
    public void clear() {
        this.cssPathList.clear();
    }

    @Override
    public List<ParsedContent> parse(String url) {
        int timeout = Integer.parseInt(WebCrawlerConfig.get("timeout"));
        Document doc;
        try {
            doc = Jsoup.connect(url).timeout(timeout).get();
        } catch (IOException ex) {
            Logger.getLogger(CSSParser.class.getName()).log(Level.SEVERE, null,
                    ex);
            return null;
        }
        Elements tmp = doc.select("body");
        if (tmp == null) {
            return null;
        }
        Element ele = tmp.first();
        return this.parse(ele, url);

    }

    @Override
    public List<ParsedContent> parse(Element ele, String url) {
        List<ParsedContent> res = new ArrayList<>();
        //TODO: przemyśleć tę listę
        ParsedContent pc = new ParsedContent(":" + url);//TODO: url
        boolean found = false;
        pc.addProperty("url", url);
        for (CSSPath css : cssPathList) {

            Elements select = ele.select(css.path);
            for (Element e : select) {
                found = true;
                switch (css.pathType) {
                    case table:
                        pc.addTable(e);
                        break;
                    case img:// "div#wszystko.tabContent table tbody tr td div p.MsoNormal img"
                        String get = ele.select(css.path).
                                get(0).attributes().get("src");
                        pc.addProperty(css.label, get);
                        break;
                    case text:
                        //TODO: replace filter
                        String text = ele.select(css.path).
                                text().replace("zł z VAT", "");
                        if (css.label.equals("cena")) {

                        }
                        pc.addProperty(css.label, text);
                        break;
                    case list:
                        pc.addList(e);
                    case list_2:
                        pc.addList2(e);
                        break;
                }

            }

        }
        if (found) {
            res.add(pc);
        }
        return res;
    }

}
