/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.results;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Tomek
 */
public class ParsedContent {

    private static final HashMap<String, String> allPropertyKeys = new HashMap<>();
    private static final List<String> keyOrder = new ArrayList<>();
    public static final Semaphore keySem = new Semaphore(1);

    public static List<String> getAllKeys() {

        return keyOrder;

    }

    HashMap<String, String> properties = new HashMap<>();

    private String Id;
    private String Url;

    public ParsedContent(String Id) {
        this.Id = Id;
    }

    public ParsedContent(String Id, String Url) {
        this.Id = Id;
        this.Url = Url;
    }

    public ParsedContent() {
    }

    public void addTable(Element ele) {
        Elements elements = ele.select("tr");
        for (Element e : elements) {
            Elements tmp = e.select("td");
            String key = null;
            if (tmp.size() > 0) {
                key = tmp.get(0).text();
            }
            if (tmp.size() > 1) {
                for (int i = 1; i < tmp.size(); i++) {
                    String val = tmp.get(1).text();
                    this.addProperty(key, val);
                }
            }
        }
    }

    public void addList(Element ele) {
        Elements elements = ele.select("li");
        for (Element e : elements) {
            this.addProperty(e.text(), "1");

        }
    }

    public void addList2(Element ele) {
        Elements elements = ele.select("li");
        for (Element e : elements) {
            if (e.childNodes().size() > 1) {
                if (e.select("small").size() > 0 && e.select("span").size() > 0) {
                    String key = e.select("small").get(0).text();
                    String val = e.select("span").get(0).text();
                    this.addProperty(key, val);
                }
            }

        }
    }

    public synchronized void addProperty(String key, String value) {
        this.properties.put(key, value);
        String put = allPropertyKeys.put(key, key);
        if (put == null) {
            try {
                keySem.acquire();
                keyOrder.add(key);

            } catch (InterruptedException ex) {
                Logger.getLogger(ParsedContent.class.getName()).
                        log(Level.SEVERE, null, ex);
            }

            keySem.release();
        }
    }

    @Override
    public String toString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String structToString() {
        //TODO: switch over result types
        String res = "id";
        try {
            keySem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(ParsedContent.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        for (String key : getAllKeys()) {
            res += "\t" + key;
        }
        keySem.release();
        return res + "\n";
    }

    public String toCSV() {
        String res = this.Id;//+ "\t" + this.Url;
        try {
            keySem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(ParsedContent.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        for (String key : getAllKeys()) {
            if (this.properties.get(key) != null) {
                res += "\t" + this.properties.get(key);
            } else {
                res += "\t";
            }
        }
        keySem.release();
        return res + "\n";
    }

    public String toJson() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String toXML() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
