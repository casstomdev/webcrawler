/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.results;

import java.util.List;
import org.jsoup.nodes.Element;

/**
 *
 * @author Tomek
 */
public abstract class ResultParser {

    /**
     *
     * @param URL
     * @return
     */
    public abstract List<ParsedContent> parse(String URL);

    /**
     *
     * @param ele
     * @return
     */
    public abstract List<ParsedContent> parse(Element ele, String url);
}
