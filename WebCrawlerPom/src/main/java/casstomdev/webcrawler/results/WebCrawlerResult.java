/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.results;

import casstomdev.webcrawler.WebCrawlerConfig;
import java.util.List; 

/**
 *
 * @author T430
 */
public class WebCrawlerResult {

    private final String url;
    private String content = "";
    private List<ParsedContent> parsedContent;
//TODO: przełacznik czy został zapisany do pliku

    public WebCrawlerResult(String url) {
        this.url = url;
    }

    public WebCrawlerResult(String url, String content) {
        this.url = url;
        this.content = content;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

//    /**
//     * return page content row url;content
//     *
//     * @return
//     */
//    public String getRow() {
//        String res = "";
//        res = this.url + ";" + encode(this.content);
//        return res;
//    }
    @Override
    public String toString() {
        if (this.parsedContent != null || WebCrawlerConfig.getParser() != null) {
            return parsed();
        } else {
            return unparsed();
        }
    }

    private String parsed() {
        String conf = WebCrawlerConfig.get("output_format");
        String res = "";
        if (this.parsedContent == null || this.parsedContent.isEmpty()) {
return null;
//            return this.url + "\n";
        }
        for (ParsedContent pc : this.parsedContent) {
            switch (conf) {
                case "csv":
                    res += pc.toCSV();break;
                default:
                    res += pc.toCSV();break;
            }

        }
        return res;

    }

    private String unparsed() {
        String conf = WebCrawlerConfig.get("output_format");
        switch (conf) {
            case "raw":
                return this.url + ";" + encode(this.content);
            default:
                return this.url + ";" + encode(this.content);
        }
    }

    /**
     * Encodes crawler result
     *
     * @param content to encode
     * @return encoded string
     */
    public static String encode(String content) {
        String encoded = "";
        for (int i = 0; i < content.length(); i++) {
            char k = content.charAt(i);
            if (k == ';') {
                encoded += "%sem";
            } else if (k == ';') {
                encoded += "%per";
            } else if (k == '\n') {
                encoded += "%enl";
            } else if (k == '\r') {
                encoded += "";
            } else {
                encoded += k;
            }

        }
        encoded += "\n";
        return encoded;
    }

    /**
     * decode crawler result
     *
     * @param content to decode
     * @return decoded string
     */
    public static String decode(String content) {
        String decoded = "";
        for (int i = 0; i < content.length(); i++) {

            char k = content.charAt(i);
            if (k == '%') {
                String sub = content.substring(i, i + 3);
                i += 3;
                switch (sub) {
                    case "sem":
                        decoded += ";";
                        break;
                    case "per":
                        decoded += "%";
                        break;
                    case "enl":
                        decoded += "\r\n";
                        break;
                }
                decoded += "%sem";

            } else {
                decoded += k;
            }

        }

        return decoded;
    }

    /**
     * @return the parsedContent
     */
    public List<ParsedContent> getParsedContent() {
        return parsedContent;
    }

    /**
     * @param parsedContent the parsedContent to set
     */
    public void setParsedContent(
            List<ParsedContent> parsedContent) {
        this.parsedContent = parsedContent;
    }
}
