/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrawler.utils;

/**
 *
 * @author T430
 */
public class WebCrawlerPathObject {

   public WebCrawlerPathObject previousStep;
   public WebCrawlerPathObject nextStep;
   private String Url;
   private String CSS;

   /**
    * step informaton
    *
    * @param url
    */
   public WebCrawlerPathObject(String url) {
      this.Url = url;

   }

   /**
    * step informaton
    *
    * @param url
    * @param css
    */
   public WebCrawlerPathObject(String url, String css) {
      this.Url = url;
      this.CSS = css;
   }

//   public WebCrawlerPathObject(String url, WebCrawlerPathObject prevPath) {
//      WebCrawlerPathObject iter = prevPath.root;
//      this.root = new WebCrawlerPathObject(iter.getUrl());
//      WebCrawlerPathObject tmp = this.root;
//      while (iter != null) {
//         tmp = tmp.addNextStep(iter.getUrl());
//         iter = iter.nextStep;
//      }
//   }
   /**
    * @return the Url
    */
   public String getUrl() {
      return Url;
   }

   /**
    * @return the CSS
    */
   public String getCSS() {
      return CSS;
   }
//   /**
//    * removes last element from list
//    *
//    * @return reference to previous obj or null when list is empty
//    */
//   public WebCrawlerPathObject removeLastStep() {
//      if (this.getUrl().equals(this.root.getUrl())) {
//
//         return null;
//      } else {
//         this.previousStep.nextStep = null;
//        
//         return this.previousStep;
//      }
//
//   }
   /**
    * deletes all elements from path list accept first one
    *
    * @return
    */
//   public WebCrawlerPathObject clear() {
//      WebCrawlerPathObject tmp = this.root;
//      while (tmp.nextStep != null) {
//         tmp = tmp.nextStep;
//      }
//      while(tmp!=null){
//         tmp=tmp.removeLastStep();
//      }
//      return new WebCrawlerPathObject(this.root.Url);
//
//
//   }
}
