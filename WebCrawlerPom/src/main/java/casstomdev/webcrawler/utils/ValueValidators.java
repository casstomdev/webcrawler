/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author T430
 */
public class ValueValidators {

   /**
    * check if input value is correct int
    *
    * @param value value to check
    * @param min greater then min
    * @param max less then max
    * @param msg error msg
    * @return true if no error
    *
    * set max less then min for no upper bound <br/> set min equal max for no lower bound
    */
   public static boolean intcheck(String value, int min, int max, String msg) throws Exception {
      try {
         int num = Integer.parseInt(value);
         if (min < num && num < max) {
            return true;
         } else if (max < min && min < num) {
            return true;
         } else if (min == max && max > num) {
            return true;
         }
      } catch (Exception e) {
         Logger.getLogger(ValueValidators.class.getName()).log(Level.SEVERE, null, msg);

      }
      throw new Exception(msg);
   }

   /**
    * check if input value is correct int
    *
    * @param value value to check
    * @param msg error msg
    * @return true if no error
    */
   public static boolean intcheck(String value, String msg) throws Exception {
      try {
         Integer.parseInt(value);
         return true;
      } catch (Exception e) {
         Logger.getLogger(ValueValidators.class.getName()).log(Level.SEVERE, null, msg);

      }
      throw new Exception(msg);
   }

   /**
    * check if input value is correct int
    *
    * @param value value to check
    * @param min greater then min
    * @param max less then max
    * @param msg error msg
    * @return true if no error
    *
    * set max less then min for no upper bound <br/> set min equal max for no lower bound
    */
   public static boolean doublecheck(String value, double min, double max, String msg) throws Exception {
      try {
         double num = Double.parseDouble(value);
         if (min < num && num < max) {
            return true;
         } else if (max < min && min < num) {
            return true;
         } else if (min == max && max > num) {
            return true;
         }
      } catch (Exception e) {
         Logger.getLogger(ValueValidators.class.getName()).log(Level.SEVERE, null, msg);

      }
      throw new Exception(msg);
   }

   /**
    * check if input value is correct int
    *
    * @param value value to check
    * @param msg error msg
    * @return true if no error
    */
   public static boolean doublecheck(String value, String msg) throws Exception {
      try {
         Double.parseDouble(value);
         return true;
      } catch (Exception e) {
         Logger.getLogger(ValueValidators.class.getName()).log(Level.SEVERE, null, msg);

      }
      throw new Exception(msg);
   }
   

}
