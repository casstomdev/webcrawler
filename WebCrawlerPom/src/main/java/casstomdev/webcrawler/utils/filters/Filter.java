/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.utils.filters;

/**
 *
 * @author T430
 */
public class Filter {

   /**
    * @return the Regex
    */
   public String getRegex() {
      return Regex;
   }

   /**
    * @return the Regex
    */
   void setRegex(String value) {
      Regex = value;
   }

   /**
    * @return the Type
    */
   public filterType getType() {
      return Type;
   }

   public enum filterType {

      css, regex, crawler
   };
   private String Regex;
   private filterType Type;

   public Filter(String Regex, filterType Type) {
      this.Regex = Regex;
      this.Type = Type;
   }
}
