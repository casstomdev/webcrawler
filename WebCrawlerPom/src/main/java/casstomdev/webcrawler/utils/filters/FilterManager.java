/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.utils.filters;

import casstomdev.webcrawler.WebCrawler;
import casstomdev.webcrawler.WebCrawlerConfig;
import casstomdev.webcrawler.logger.WCLogger;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 *
 * @author T430
 */
public class FilterManager {

    public Method urlFilter = null;
    public Method contentFilter = null;

    /**
     * creates default filter file
     *
     * @param filterPath previous file name
     * @return new file name
     */
    private static String createFilter(String filterPath) {
        FileWriter fw = null;
        try {

            WCLogger.getLogger().error("file: " + filterPath + " not found, used default settings");
            fw = new FileWriter("filters.config");
            fw.write("bcrawler;<domain>/.*\\.(pdf|jpeg|png)\n");
            fw.write("pcrawler;<domain>/.*");

            filterPath = "filters.config";

        } catch (IOException ex) {
            WCLogger.getLogger().logException(ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                WCLogger.getLogger().logException(ex);
            }
        }
        return filterPath;
    }

    /**
     * reads filter from file to array list
     *
     * @param filterPath path to file
     */
    private static void openFillterFile(String filterPath) {
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(filterPath);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                Filter filter = null;
                char type = strLine.charAt(0);
                strLine = strLine.substring(1);
                if (type != '#') {
                    if (strLine.startsWith("crawler;")) {
                        filter = new Filter(strLine.substring("crawler;".length()), Filter.filterType.crawler);
                    } else if (strLine.startsWith("css;")) {
                        filter = new Filter(strLine.substring("css;".length()), Filter.filterType.css);
                    } else if (strLine.startsWith("regex;")) {
                        filter = new Filter(strLine.substring("regex;".length()), Filter.filterType.regex);
                    }

                    if (type == 'p') {
                        filters.add(filter);
                    } else {
                        blockFilters.add(filter);
                    }
                }
            }
            in.close();
        } catch (IOException ex) {
            WCLogger.getLogger().logException(ex);
        } finally {
            try {
                fstream.close();
            } catch (IOException ex) {
                WCLogger.getLogger().logException(ex);
            }
        }

    }
    //TODO: ignore filters for url get parameters
    static ArrayList<Filter> filters = new ArrayList<>();
    static ArrayList<Filter> blockFilters = new ArrayList<>();

    /**
     * imports url filters from file
     */
    public static void importUrlFilter() {

        String filterPath = WebCrawlerConfig.get("filter_path");
        File file = new File(filterPath);
        if (!file.exists()) {
            filterPath = createFilter(filterPath);

        }
        openFillterFile(filterPath);
    }

    /**
     * gets size of filters list
     *
     * @return
     */
    public static int getSize() {
        return filters.size();
    }

    /**
     * get filter regexp from list
     *
     * @param idx filter index
     * @return filter ot null if idx is wrong
     */
    public static Filter getFilter(int idx) {
        if (idx > 0 && idx < getSize()) {
            return filters.get(idx);
        }
        return null;
    }

    /**
     * filter url
     *
     * @param url url to filter
     * @param crawler inicjator
     * @return true if url is ok, false when url should be filtered
     */
    public static boolean checkURL(String url, WebCrawler crawler) {
        boolean result = false;
//      if (url.contains("http://weather.yahoo.com/forecast/Poznan_PL_c.html")) {
//         System.out.println("ddd sss");
//      }
        url = url.replace("www.", "");
        url = url.replace("http://", "");
        if (url.contains("#")) {
            url = url.substring(0, url.lastIndexOf('#'));
        }
        //http://moto.gratka.pl/osobowe/?s=2320
        //
        result = checkBlockFilters(url);
        if (!result) {
            return result;
        }
        result = false;
        for (Filter filter : filters) {
            if (filter != null) {
                if (filter.getType() == Filter.filterType.crawler) {
                    result = result | CrawlerFilter.checkURL(url, filter.getRegex());
                }
            }
            if (result) {
                return result;
            }
        }
        //TODO: logic here
        return result;
    }

    private static boolean checkBlockFilters(String url) {
        boolean result = false;
        for (Filter filter : blockFilters) {
            if (filter != null) {
                if (filter.getType() == Filter.filterType.crawler) {
                    result = result | CrawlerFilter.checkURL(url, filter.getRegex());
                }
            }
            if (result) {
                return !result;
            }
        }
        return !result;
    }
}
