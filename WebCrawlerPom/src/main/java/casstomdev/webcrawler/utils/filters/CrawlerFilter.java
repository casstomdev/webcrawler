/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler.utils.filters;


/**
 *
 * @author T430
 */
public class CrawlerFilter {

   /**
    * filters given url
    *
    * @param url
    * @param filter filter value
    * @return true if url is ok, false -> url should be omitted
    */
   public static boolean checkURL(String url, String filter) {
      return url.matches(filter);
   }

   /**
    * designates filters
    *
    * @param domain
    * @param startUrl
    */
   public static void designateFilters(String domain, String startUrl) {
      for (Filter filter : FilterManager.blockFilters) {
         if (filter.getType() == Filter.filterType.crawler) {
            filter.setRegex(filter.getRegex().replace("<domain>", domain));
            filter.setRegex(filter.getRegex().replace("<start_url>", startUrl));
         }
      }
      for (Filter filter : FilterManager.filters) {
         if (filter.getType() == Filter.filterType.crawler) {
            filter.setRegex(filter.getRegex().replace("<domain>", domain));
            filter.setRegex(filter.getRegex().replace("<start_url>", startUrl));
         }
      }
   }
}
