/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrawler.utils;

/**
 *
 * @author T430
 */
public class WebCrawlerPath {

   WebCrawlerPathObject Current;
   WebCrawlerPathObject Head;
   int size;

   public WebCrawlerPath() {
   }

   /**
    * copies pirevious path elements into current list
    *
    * @param prevPath
    */
   public WebCrawlerPath(WebCrawlerPath prevPath) {
      WebCrawlerPathObject iter = prevPath.getFirst();
      while (iter != null) {

         if (size == 0) {
            Head = new WebCrawlerPathObject(iter.getUrl());
            Current = Head;
            size++;

         } else {
            //TODO: url check
            WebCrawlerPathObject nextStep = new WebCrawlerPathObject(iter.getUrl());
            Current.nextStep = nextStep;
            nextStep.previousStep = Current;
            Current = nextStep;
            size++;

         }
         iter = iter.nextStep;
      }
   }

   /**
    * return first step
    *
    * @return
    */
   public WebCrawlerPathObject getFirst() {
      return this.Head;
   }

   public WebCrawlerPathObject getCurrent() {
      return this.Current;
   }

   /**
    * get path length
    *
    * @return
    */
   public int size() {
      return this.size;
   }

   /**
    * deletes all elements from path list accept first one
    *
    * @return
    */
   @SuppressWarnings("empty-statement")
   public void clear() {
      WebCrawlerPathObject tmp = this.Head;
      while (removeLast());
      this.add(tmp.getUrl());

   }

   private static boolean addStep(WebCrawlerPath path) {
      return false;
   }

   /**
    * add step to list
    *
    * @param url
    * @return true if added, current step equals recenlty added step
    */
   public boolean add(String url) {

      if (size == 0) {
         Head = new WebCrawlerPathObject(url);
         Current = Head;
         size++;
         return true;
      } else {
         //TODO: url check
         WebCrawlerPathObject nextStep = new WebCrawlerPathObject(url);
         Current.nextStep = nextStep;
         nextStep.previousStep = Current;
         Current = nextStep;
         size++;
         return true;
      }
      //    return false;
   }

   /**
    * removes last step from path
    *
    * @return true if element was removed, false, list is empty
    */
   public boolean removeLast() {
      if (Current.getUrl().equals(this.Head.getUrl())) {
         Current = null;
         Head = null;
         size = 0;
         return false;
      } else {
         WebCrawlerPathObject prevStep = this.Current.previousStep;
         prevStep.nextStep = null;
         this.Current = null;
         this.Current = prevStep;
         this.size--;
         return true;
      }

   }
}
