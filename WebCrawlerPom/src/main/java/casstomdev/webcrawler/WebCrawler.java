/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package casstomdev.webcrawler;

import casstomdev.webcrawler.utils.filters.FilterManager;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import casstomdev.webcrawler.logger.RecoverLog;
import casstomdev.webcrawler.logger.WCLogger;
import casstomdev.webcrawler.logger.WebCrawlerLogger;
import casstomdev.webcrawler.results.ParsedContent;
import casstomdev.webcrawler.results.ResultParser;
import casstomdev.webcrawler.results.WebCrawlerResult;
import casstomdev.webcrawler.utils.filters.CrawlerFilter;
import casstomdev.webcrawler.workers.WorkerManager;
import webcrawler.utils.WebCrawlerPath;

/**
 *
 * @author T430
 */
public class WebCrawler {

    protected WebCrawlerResultManager WCRM;
    protected WorkerManager WM;
    protected String HostName;
    protected String Start;

    /**
     * init crawler with default config
     */
    public WebCrawler() {
        crawlerInit("default.config");
        WM = new WorkerManager(this);
    }

    /**
     * read config from given file
     *
     * @param path path to file
     */
    public WebCrawler(String path) {
        crawlerInit(path);
        WM = new WorkerManager(this);
    }

    /**
     * start crawling page
     *
     * @param startPoint
     */
    protected void Crawl(String startPoint) {

        _Crawl(startPoint);
    }

    /**
     * restart crawling page
     *
     * @param startPoint
     */
    protected void Restart(String startPoint) {

        _Restart(startPoint);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        // WCLogger.saveLogs();
    }

    //jakk przydzielac zadania w watkach - dzieciach jak dojedzie do konca listy url
    /**
     * WebCrawlerResultManager
     *
     * @return result from previuos Crawling
     */
    public WebCrawlerResultManager getCrawlingResults() {
        return this.WCRM;
    }

    /**
     * init webcrawler
     *
     * @param configFile
     */
    private void crawlerInit(String configFile) {

        WebCrawlerConfig.openConfigFile(configFile);
        WebCrawlerConfig.init();
        this.WCRM = new WebCrawlerResultManager(Integer.parseInt(
                WebCrawlerConfig.get("max_page_limit")));
        if (WebCrawlerConfig.get("webcrawler_url_filter").equals("1")) {
            FilterManager.importUrlFilter();
        }

    }

    /**
     * @return the WM
     */
    public WorkerManager getWM() {
        return WM;
    }

    /**
     * get host name from input url
     *
     * @param url
     * @return hostname or null when error occured
     */
    protected String getHostName(String url) {
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain.startsWith("www.") ? domain.substring(4) : domain;

        } catch (URISyntaxException ex) {
            // WCLogger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
            WCLogger.getLogger().error("host name in: " + url + ". " + ex.getMessage());
        }
        return null;
    }

    /**
     * @return the HostName
     */
    public String getHostName() {
        return HostName;
    }

    protected void _Crawl(String startPoint) throws NumberFormatException {
        PrepareCrawler(startPoint);
        if (this.HostName == null) {
            return;
        }
        boolean putProcess = WCRM.putProcess(startPoint);
        if (putProcess) {
            WebCrawlerLogger.LogStart(startPoint);

            WebCrawlerPath path = new WebCrawlerPath();
            path.add(startPoint);
            //TODO: crawling

            WebCrawlerFunctions wcf = new WebCrawlerFunctions(path);
            String content = "";
            int depth = Integer.parseInt(WebCrawlerConfig.get("crawl_depth"));
            if (path.size() < depth) {
                if (wcf.getPage()) {
                    Elements urlList = wcf.getUrlList();

                    for (Element url : urlList) {
                        if (FilterManager.checkURL(url.attr("abs:href"), this)) {
                            WM.addTask(url.attr("abs:href"), path);
                        }
                    }
                    for (int i = 1; i < 5515; i++) {
                        //     for(int i =1;i<2324;i++){
                        //http://otomoto.pl/osobowe/?page=500
                        WM.addTask("http://otomoto.pl/osobowe/?page=" + i, path);
                        // WM.addTask("http://moto.gratka.pl/osobowe/?s="+i, path);

                    }
                    content = wcf.getPageContent();
                }
            }
            WebCrawlerResult res = new WebCrawlerResult(startPoint, content);

            if (WebCrawlerConfig.getParser() != null) {
                ResultParser parser = WebCrawlerConfig.getParser();
                List<ParsedContent> parse = parser.parse(wcf.getJsoupDocument(), wcf.getUrl());
                res.setParsedContent(parse);
            }
            WCRM.addResult(res);
            //sprawdzac czy powyzsze adresy znajduja sie w pliku logu ( obojetnie czy w process list czy finished list)
            WebCrawlerLogger.LogEnd(startPoint);

        }
        long wait = 100;
        while (WM.getPoolOccupancy() != 0) {
            try {
                Thread.sleep(wait);
                if (wait < 20000) {
                    wait += 100;
                }
                // System.out.println("Crawler is alife "+ WM.getPoolOccupancy());
            } catch (InterruptedException ex) {
                WCLogger.getLogger().logException(ex);
            }
        }
        System.out.println("Crawler is stopping: " + WM.getPoolOccupancy());
        WM.close();
        //WCLogger.saveLogs();
        WebCrawlerLogger.storeLog();

        WCRM.dumpResults();

    }

    /**
     * @return the Start
     */
    public String getStart() {
        return Start;
    }

    /**
     * prepares basic information
     *
     * @param startPoint
     */
    private void PrepareCrawler(String startPoint) {
        WCLogger.printMsg("crawling started");
        this.Start = startPoint;
        this.Start = this.Start.replace("www.", "");
        this.Start = this.Start.replace("http://", "");
        this.HostName = getHostName(startPoint);
        CrawlerFilter.designateFilters(this.HostName, this.Start);
    }

    protected void _Restart(String startPoint) {
        PrepareCrawler(startPoint);
        if (this.HostName == null) {
            return;
        }
        for (String task : RecoverLog.tasks) {

            WebCrawlerPath path = new WebCrawlerPath();
            path.add(task);
            if (FilterManager.checkURL(task, this)) { // jak było w logu to chyba niepotrzebne
                WM.addTask(task, path);
            }
            //TODO: crawling
        }
        long wait = 100;
        while (WM.getPoolOccupancy() != 0) {
            try {
                Thread.sleep(wait);
                if (wait < 10000) {
                    wait += 100;
                }
            } catch (InterruptedException ex) {
                WCLogger.getLogger().logException(ex);
            }
        }
        WM.close();
        //WCLogger.saveLogs();
        WebCrawlerLogger.storeLog();
        WCRM.dumpResults();
    }
}
